package org.peacewing.core;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.domain.ConfigFile;

public class SpringMVCXml extends ConfigFile{
	protected String xmlDefinition = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	protected String packageToken;
	protected String content() { return  "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n"+
			"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n"+
			"xmlns:mvc=\"http://www.springframework.org/schema/mvc\"\n"+
			"xsi:schemaLocation=\"http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.1.xsd\n"+
				"\thttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.1.xsd\n"+
				"\thttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.1.xsd\">\n\n"+
			"<context:component-scan\n"+
				"\tbase-package=\""+this.getPackageToken()+".facade\"\n"+
				"\tuse-default-filters=\"false\">\n"+
				"\t<context:include-filter type=\"annotation\"\n"+
					"\t\texpression=\"org.springframework.stereotype.Controller\" />\n"+
				"\t<context:include-filter type=\"annotation\"\n"+
					"\t\texpression=\"org.springframework.web.bind.annotation.ControllerAdvice\" />\n"+
			"</context:component-scan>\n\n"+
			"<mvc:annotation-driven>\n"+
				"\t<mvc:message-converters register-defaults=\"true\">\n"+
					"\t\t<bean class=\"org.springframework.http.converter.StringHttpMessageConverter\">\n"+
						"\t\t\t<constructor-arg value=\"UTF-8\" />\n"+
					"\t\t</bean>\n"+
					"\t\t<bean\n"+
						"\t\t\tclass=\"org.springframework.http.converter.json.MappingJackson2HttpMessageConverter\">\n"+
						"\t\t\t<property name=\"prettyPrint\" value=\"true\" />\n"+
						"\t\t\t<property name=\"objectMapper\">\n"+
							 "\t\t\t\t<bean class=\"com.fasterxml.jackson.databind.ObjectMapper\" />\n"+                       
					    "\t\t\t</property>\n"+				
					"\t\t</bean>\n"+
				"\t</mvc:message-converters>\n"+
			"</mvc:annotation-driven>\n"+
			"</beans>\n";
	}
	
	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}

	public SpringMVCXml(){
		super();
		this.standardName = "spring-mvc.xml";
	}
	
	@Override 
	public void setStandardName(String standardName){
	}
	
	@Override
	public String generateConfigFileString() {
		StringBuilder sb = new StringBuilder();
		sb.append(xmlDefinition);
		sb.append(content());
		return sb.toString();
	}

}
