package org.peacewing.include;

import org.peacewing.domain.Include;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.utils.StringUtil;
import org.peacewing.verb.ListAllByPage;

public class UserNav extends Include{	
	public UserNav(){
		super();
		this.fileName = "usernav.jsp";
		this.packageToken = "";
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!-- Common Navigation Panel for our site -->\n");
		sb.append("<li id=\"submenu\">\n");
		sb.append("\t<h2><a href=\"../index.html\">Homepage</a></h2><br/>\n");
		sb.append("\t<h2><a href=\"../pages/index.html\">Json UI</a></h2><br/>\n");
		sb.append("\t<h2><a>Select an option</a></h2>\n");
		sb.append("\t<ul>\n");
		for (ListAllByPage lsa : this.allListAllByPageList){
			sb.append("\t\t<li><a href=\""+"../controller/"+StringUtil.lowerFirst(lsa.getVerbName())+"Controller\">"+lsa.getVerbName()+"</a></li>\n");   
		}
		sb.append("\t</ul>\n");
		sb.append("</li>\n");
		return sb.toString();
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		StatementList list = new StatementList();
		list.setSerial(serial);
		list.addStatement(new Statement(1000L,indent,"<!-- Common Navigation Panel for our site -->\n"));
		list.add(new Statement(2000L,indent,"<li id=\"submenu\">"));
		list.add(new Statement(3000L,indent+1,"<h2><a href=\"../index.html\">Homepage</a></h2><br/>"));
		list.add(new Statement(4000L,indent+1,"<h2><a href=\"../jsonjsp/index.jsp\">Json UI</a></h2><br/>"));
		list.add(new Statement(5000L,indent+1,"<h2><a>Select an option</a></h2>"));
		list.add(new Statement(6000L,indent+1,"<ul>"));
		long myserial = 7000L;
		for (ListAllByPage lsa : this.allListAllByPageList){
			list.add(new Statement(myserial,indent+2,"<li><a href=\""+"../controller/"+StringUtil.lowerFirst(lsa.getVerbName())+"Controller\">"+lsa.getVerbName()+"</a></li>\n"));
			myserial+=1000L;
		}
		list.add(new Statement(myserial,indent+1,"</ul>\n"));
		list.add(new Statement(myserial+1000L,indent,"</li>\n"));
		return list;
	}
}
