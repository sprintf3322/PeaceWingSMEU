package org.peacewing.generator;

public class DBDefinitionFactory {
	public static DBDefinitionGenerator getInstance(String type){
		switch (type) {
		case "mysql" : return new MysqlDBDefinitionGenerator();
		default : return null;
		}
	}
}
