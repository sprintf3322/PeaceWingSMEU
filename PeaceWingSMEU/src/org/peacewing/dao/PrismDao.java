package org.peacewing.dao;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Naming;
import org.peacewing.domain.Prism;

public interface PrismDao {
	public Prism generatePrism(Naming naming, String standardName,Domain domain) throws Exception;
	public void generatePrismFiles(Prism prism);
}
