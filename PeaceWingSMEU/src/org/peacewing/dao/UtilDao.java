package org.peacewing.dao;

import org.peacewing.domain.Naming;

public interface UtilDao {
	public String generateUtilString(Naming naming, String standardName) throws Exception;

}
