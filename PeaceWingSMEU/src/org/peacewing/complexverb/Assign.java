package org.peacewing.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.DragonHideStatement;
import org.peacewing.domain.Field;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.utils.MybatisSqlReflector;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class Assign extends TwoDomainVerb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.setStandardName("assign"+this.slave.getCapFirstDomainName()+"To"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("assign"+StringUtil.capFirst(this.slave.getAlias())+"To"+this.master.getCapFirstDomainName());
		}
		method.setNoContainer(true);
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,1,"<insert id=\""+method.getStandardName()+"\">"));
		list.add(new Statement(200L,2, MybatisSqlReflector.generateInsertLinkTwoSql(master,slave)));
		list.add(new Statement(300L,1,"</insert>"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.setStandardName("assign"+this.slave.getCapFirstDomainName()+"To"+this.master.getCapFirstDomainName());		
		} else {
			method.setStandardName("assign"+StringUtil.capFirst(this.slave.getAlias())+"To"+this.master.getCapFirstDomainName());
		}
		method.setReturnType(new Type("Integer"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){			
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Id", this.slave.getDomainId().getClassType()));
		}else{
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Id", this.slave.getDomainId().getClassType()));
		}
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){		
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		}else{
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String")));
		}
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.springframework.transaction.annotation.Transactional");
		method.addAdditionalImport(this.master.getPackageToken()+".dao."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+".service."+this.slave.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		if (StringUtil.isBlank(this.slave.getAlias())){		
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		}else {		
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String")));
		}
		method.addMetaData("Override");	
		method.addMetaData("Transactional");	
		
		List<Writeable> list = new ArrayList<Writeable>();
		if (StringUtil.isBlank(this.slave.getAlias())){	
			list.add(new Statement(1000L,2,"String [] "+this.slave.getLowerFirstDomainName()+"IdsArr = "+this.slave.getLowerFirstDomainName()+"Ids.split(\",\");"));
			list.add(new Statement(2000L,2,"for (int i = 0; i < "+this.slave.getLowerFirstDomainName()+"IdsArr.length; i++ ){"));
			list.add(new DragonHideStatement(3000L,2,"String "+this.slave.getLowerFirstDomainName()+"Id = "+this.slave.getLowerFirstDomainName()+"IdsArr[i];",this.slave.getDomainId().getClassType().toString().equals("String")));
			list.add(new DragonHideStatement(3000L,2,"Long "+this.slave.getLowerFirstDomainName()+"Id = Long.parseLong("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Long")));
			list.add(new DragonHideStatement(3000L,2,"Integer "+this.slave.getLowerFirstDomainName()+"Id = Integer.parseInt("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Integer")));
			list.add(new Statement(4000L,2,"Integer success = dao."+generateDaoMethodDefinition().generateStandardCallString()+";"));
			list.add(new Statement(5000L,2,"if (success < 0) return false;"));
			list.add(new Statement(6000L,2,"}"));
			list.add(new Statement(7000L,2,"return true;"));
		} else {
			list.add(new Statement(1000L,2,"String [] "+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr = "+StringUtil.lowerFirst(this.slave.getAlias())+"Ids.split(\",\");"));
			list.add(new Statement(2000L,2,"for (int i = 0; i < "+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr.length; i++ ){"));
			list.add(new DragonHideStatement(3000L,2,"String "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = "+ StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i];",this.slave.getDomainId().getClassType().toString().equals("String")));
			list.add(new DragonHideStatement(3000L,2,"Long "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = Long.parseLong("+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Long")));
			list.add(new DragonHideStatement(3000L,2,"Integer "+StringUtil.lowerFirst(this.slave.getAlias())+"Id = Integer.parseInt("+StringUtil.lowerFirst(this.slave.getAlias())+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Integer")));
			list.add(new Statement(4000L,2,"Integer success = dao."+generateDaoMethodDefinition().generateStandardCallString()+";"));
			list.add(new Statement(5000L,2,"if (success < 0) return false;"));
			list.add(new Statement(6000L,2,"}"));
			list.add(new Statement(7000L,2,"return true;"));
		}
		
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateFacadeMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.master.getPackageToken()+".service."+this.master.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType(),"RequestParam(value = \""+this.master.getLowerFirstDomainName()+"Id\", required = true)"));
		if (StringUtil.isBlank(this.slave.getAlias())){	
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String"),"RequestParam(value = \""+this.slave.getLowerFirstDomainName()+"Ids\", required = true)"));
		}else {
			method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getAlias())+"Ids", new Type("String"),"RequestParam(value = \""+StringUtil.lowerFirst(this.slave.getAlias())+"Ids\", required = true)"));
		}
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(getVerbName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		wlist.add(new Statement(1000L, 2, "Map<String,Object> result = new TreeMap<String,Object>();"));
		wlist.add(new Statement(2000L, 2, "boolean success = service."+generateServiceMethodDefinition().generateStandardCallString()+";"));
		wlist.add(new Statement(3000L, 2, "result.put(\"success\",success);"));
		wlist.add(new Statement(4000L, 2, "result.put(\"rows\",null);"));
		wlist.add(new Statement(5000L, 2, "return result;"));
		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
		
	public Assign(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		if (StringUtil.isBlank(this.slave.getAlias())){	
			this.setVerbName("Assign"+this.slave.getCapFirstPlural()+"To"+this.master.getCapFirstDomainName());
		}else {
			this.setVerbName("Assign"+StringUtil.capFirst(this.slave.getAliasPlural())+"To"+this.master.getCapFirstDomainName());
		}
		this.setLabel("分配");
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		
		StatementList sl = new StatementList();
		if (StringUtil.isBlank(this.slave.getAlias())){	
			sl.add(new Statement(1000,1, "var "+this.slave.getLowerFirstDomainName()+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#myAvailable"+this.slave.getCapFirstPlural()+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择可添加"+this.slave.getText()+"！\");"));
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, this.slave.getLowerFirstDomainName()+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../facade/"+this.master.getLowerFirstDomainName()+"Facade/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, this.slave.getLowerFirstDomainName()+"Ids:"+this.slave.getLowerFirstDomainName()+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));
		} else {
			sl.add(new Statement(1000,1, "var "+StringUtil.lowerFirst(this.slave.getAlias())+"Ids = new Array();"));
			sl.add(new Statement(2000,1, "var rows = $(\"#myAvailable"+StringUtil.capFirst(this.slave.getAliasPlural())+"\").datalist(\"getSelections\");"));
			sl.add(new Statement(3000,1, "if (isBlank(rows)) {"));
			sl.add(new Statement(4000,1, "$.messager.alert(\"操作提示\", \"请选择可添加"+this.slave.getAliasText()+"！\");"));
			sl.add(new Statement(5000,1, "return;"));
			sl.add(new Statement(6000,1, "}"));
			sl.add(new Statement(7000,1, "for (var i = 0;i<rows.length;i++){"));
			sl.add(new Statement(8000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids[i] = rows[i][\""+this.slave.getDomainId().getLowerFirstFieldName()+"\"]; "));
			sl.add(new Statement(9000,1, "}"));
			sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"getSelected\")."+this.master.getDomainId().getLowerFirstFieldName()+";"));
			sl.add(new Statement(11000,1, "$.ajax({"));
			sl.add(new Statement(12000,1, "type: \"post\","));
			sl.add(new Statement(13000,1, "url: \"../facade/"+this.master.getLowerFirstDomainName()+"Facade/"+StringUtil.lowerFirst(this.getVerbName())+"\","));
			sl.add(new Statement(14000,1, "data: {"));
			sl.add(new Statement(15000,1, StringUtil.lowerFirst(this.slave.getAlias())+"Ids:"+StringUtil.lowerFirst(this.slave.getAlias())+"Ids.join(\",\"),"));
			sl.add(new Statement(16000,1, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
			sl.add(new Statement(17000,1, "},"));
			sl.add(new Statement(18000,1, "dataType: 'json',"));
			sl.add(new Statement(19000,1, "success: function(data, textStatus) {"));
			sl.add(new Statement(20000,1, "if (data.success) $(\"#my"+this.master.getCapFirstDomainName()+"\").datalist(\"load\");"));
			sl.add(new Statement(21000,1, "},"));
			sl.add(new Statement(22000,1, "complete : function(XMLHttpRequest, textStatus) {"));
			sl.add(new Statement(23000,1, "},"));
			sl.add(new Statement(24000,1, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
			sl.add(new Statement(25000,1, "alert(\"Error:\"+textStatus);"));
			sl.add(new Statement(26000,1, "alert(errorThrown.toString());"));
			sl.add(new Statement(27000,1, "}"));
			sl.add(new Statement(28000,1, "});"));			
		}
		
		method.setMethodStatementList(sl);
		return method;	
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
