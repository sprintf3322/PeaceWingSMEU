package org.peacewing.domain;

public class BooleanUtil extends Util{
	public BooleanUtil(){
		super();
		super.fileName = "BooleanUtil.java";
	}
	
	public BooleanUtil(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "BooleanUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StringBuilder sb = new StringBuilder();
		sb.append("package "+this.getPackageToken()+".utils;\n");
		sb.append("\n");
		sb.append("public class BooleanUtil {\n");
		sb.append("\n");
		sb.append("\tpublic static Boolean parseBoolean(String value){\n");
		sb.append("\t\tif (\"true\".equalsIgnoreCase(value)) return true;\n");
		sb.append("\t\telse if (\"false\".equalsIgnoreCase(value)) return false;\n");
		sb.append("\t\telse if (\"Y\".equalsIgnoreCase(value)) return true;\n");
		sb.append("\t\telse if (\"N\".equalsIgnoreCase(value)) return false;\n");
		sb.append("\t\telse if (\"T\".equalsIgnoreCase(value)) return true;\n");
		sb.append("\t\telse if (\"F\".equalsIgnoreCase(value)) return false;\n");
		sb.append("\t\telse if (\"yes\".equalsIgnoreCase(value)) return true;\n");
		sb.append("\t\telse if (\"no\".equalsIgnoreCase(value)) return false;\n");
		sb.append("\t\telse if (\"1\".equalsIgnoreCase(value)) return true;\n");
		sb.append("\t\telse if (\"0\".equalsIgnoreCase(value)) return false;\n");
		sb.append("\t\telse return null;\n");
		sb.append("\t}\n\n");
		
		sb.append("\tpublic static Integer parseBooleanInt(String value){\n");
		sb.append("\t\tBoolean b = parseBoolean(value);\n");
		sb.append("\t\tif (b==null) return null;\n");
		sb.append("\t\telse if (b==true) return 1;\n");
		sb.append("\t\telse return 0;\n");
		sb.append("\t}\n");
		sb.append("}\n");

		return sb.toString();
	}
}
