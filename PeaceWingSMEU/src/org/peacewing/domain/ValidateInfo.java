package org.peacewing.domain;

import java.util.ArrayList;
import java.util.List;

public class ValidateInfo {
	protected boolean success = true;
	protected List<String> compileErrors = new ArrayList<String>();
	protected List<String> compileWarnings = new ArrayList<String>();
	protected List<String> compileErrorsEn = new ArrayList<String>();
	protected List<String> compileWarningsEn = new ArrayList<String>();
	
	public List<String> getCompileErrorsEn() {
		return compileErrorsEn;
	}

	public void setCompileErrorsEn(List<String> compileErrorsEn) {
		this.compileErrorsEn = compileErrorsEn;
	}

	public List<String> getCompileWarningsEn() {
		return compileWarningsEn;
	}

	public void setCompileWarningsEn(List<String> compileWarningsEn) {
		this.compileWarningsEn = compileWarningsEn;
	}

	public boolean success(){
		if (this.success == false) return false; 
		if (this.compileErrors !=null && this.compileErrors.size() > 0) return false;
		else return true;
	}
	
	public List<String> getCompileErrors(){
		return this.compileErrors;
	}

	public boolean isSuccess() {
		return success();
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<String> getCompileWarnings() {
		return compileWarnings;
	}

	public void setCompileWarnings(List<String> compileWarnings) {
		this.compileWarnings = compileWarnings;
	}

	public void setCompileErrors(List<String> compileErrors) {
		this.compileErrors = compileErrors;
	}
	
	public void clearCompileErrors(){
		this.compileErrors = new ArrayList<String>();
	}
	
	public void clearCompileWarnings(){
		this.compileWarnings = new ArrayList<String>();
	}
	
	public void addCompileError(String compileError){
		this.compileErrors.add(compileError);
	}
	
	public void addAllCompileErrors(List<String> compileErrors){
		this.compileErrors.addAll(compileErrors);
	}
	
	public void addCompileWarning(String compileWarning){
		this.compileWarnings.add(compileWarning);
	}
	
	public void addAllCompileWarnings(List<String> compileWarnings){
		this.compileWarnings.addAll(compileWarnings);
	}
	
	public void addCompileErrorEn(String compileErrorEn){
		this.compileErrorsEn.add(compileErrorEn);
	}
	
	public void addAllCompileErrorsEn(List<String> compileErrorsEn){
		this.compileErrorsEn.addAll(compileErrorsEn);
	}
	
	public void addCompileWarningEn(String compileWarningEn){
		this.compileWarningsEn.add(compileWarningEn);
	}
	
	public void addAllCompileWarningsEn(List<String> compileWarningsEn){
		this.compileWarningsEn.addAll(compileWarningsEn);
	}
	
	public static ValidateInfo mergeValidateInfo(List<ValidateInfo> list){
		ValidateInfo info = new ValidateInfo();
		for (ValidateInfo iinfo : list){
			info.addAllCompileErrors(iinfo.getCompileErrors());
			info.addAllCompileWarnings(iinfo.getCompileWarnings());
			
			//English
			info.addAllCompileErrorsEn(iinfo.getCompileErrorsEn());
			info.addAllCompileWarningsEn(iinfo.getCompileWarningsEn());
		}
		for (ValidateInfo iinfo : list){
			if (iinfo.isSuccess() == false) info.setSuccess(false);
		}
		return info;
	}
}
