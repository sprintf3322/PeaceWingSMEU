package org.peacewing.utils;

import java.util.List;

import org.peacewing.domain.Domain;
import org.peacewing.exception.ValidateException;

public class DomainUtil {
	public static Domain findDomainInList(List<Domain> targets, String domainName) throws ValidateException{
		for (Domain d: targets){
			if (d.getStandardName().equals(domainName)) return d;
		}
		throw new ValidateException("域对象"+domainName+"不在列表中！");
	}
	
	public static Boolean inDomainList(Domain d,List<Domain> list){
		for (Domain dn :list){
			if (d.getStandardName().equals(dn.getStandardName())){
				return true;
			}
		}
		return false;
	}
}
