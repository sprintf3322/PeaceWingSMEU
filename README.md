# 和平之翼代码生成器SMEU版

### 最新研发动态

和平之翼代码生成器SMEU 4.0 宝船(Treasure Ship)的第一个Beta版已公布，欢迎在本站附件处下载Beta版二进制war包。

### 4.0.0 新特性清单

现在宝船已支持： 

1. 高低两种分辨率的UI
1. 个性化题头，副题头和页脚
1. 支持跨域
1. 支持两个对象间的多重多对多关系：比如一个论坛中的主题和用户之间存在多重多对多关系：点赞和收藏
1. 升级至Spring框架至4.2版。
1. 宝船的Excel模板代码生成支持三种Office:MS Office, WPS Office和Libreffice,模板需保存成xls格式
1. 宝船增加了激活和批激活两个动词
1. 需要注意，宝船代码生成器的编译兼容性为JDK 8，生成物仍然兼容JDK 
1. SGS脚本中支持双引号括起来的字符串

### 最近工作重点
近期的工作重点是发布和平之翼代码生成器SMEU 4.0.0 宝船的正式版。而后开发重点将转向无垠式代码生成器JEEEU版2.0 Elsa冰雪女王，在此版本上将实现大家期待已久的登录功能，弹性模块和名词性动词。

### 本代码生成器特色

本代码生成器是超级语言（SGS 标准生成器脚本）驱动的先进编译系统。旨在演示数据驱动的代码生成器固有的生产率上的优势和与标准编译器（Java语言）的良好协作关系。在未来，更先进的代码生成器和编译器的组合会显现出巨大的生产力优势，让我们一起促成这一天所需要的技术的进化循环。

### 用户注意

注意，本作品为火鸟（Rocketship 沈戟峰）个人作品，为开源的代码生成器，并不收取费用，也未曾委托其他的公司，如果有公司声称是它的作品，并进行网络推广活动和收取费用，皆不属实，希望所有用户注意。

### 项目代号宝船的图片

![输入图片说明](https://images.gitee.com/uploads/images/2018/0708/224107_74d3ac15_1203742.jpeg "TreasureShip.jpg")

### 最新稳定版

宝船已在主干版本上启动，希望下载3.2.0乌篷船正式版的用户请至发行版下载3.2.0版，请见下面链接下载：
[https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0](https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0)

### 乌篷船
和平之翼Java代码生成器SMEU 3.2.0版，研发代号(乌篷船 Black Awning Boat)已发布，本软件是旗舰版的动词算子式代码生成器。在附件中提供war包下载。此版本支持使用Excel模板的代码生成，支持初始数据导入，支持SGS语言语法加亮。欢迎使用。

乌篷船支持华丽的Excel模板代码生成和初始数据导入，软件新增Excel生成界面和示例Excel模板。界面和Excel模板如下图。

此分支为无垠式/和平之翼代码生成器阵列三大旗舰分支之一,其他两支为无垠式代码生成器JEEEU版和和平之翼代码生成器SHCEU版。

### 3.2.0版及特性清单

目前和平之翼代码生成器SMEU 3.2.0版乌篷船正式版已发布。此版本支持：


1. Service，Dao组件扫描
1. SGS初始数据导入
1. 多对多初始数据导入
1. 一对多动态标签
1. 新增在线文档：代码生成器技术乱弹


欢迎使用。现在此版本war包已上传至本站附件栏，并经过详细测试，有重大Bug修复与功能更新，涉及一系列遗漏的Bug修复，包括Excel模板生成的一些缺陷，和Boolean类型的一系列缺陷，已发现的Bug均已修复，欢迎下载使用：  [https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files](https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files)

3.2.0正式版源码下载请下载3.2.0的发行版，请至：[https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0](https://gitee.com/jerryshensjf/PeaceWingSMEU/releases/RELEASE_3_2_0)

本站附件中有使本生成器支持MySQL 8.0的补丁，感谢QQ好友100100101的贡献，下载请至：
[https://gitee.com/jerryshensjf/PeaceWingSMEU/attach_files](http://[输入链接说明](http://))

### 项目截屏

乌篷船：

![输入图片说明](https://gitee.com/uploads/images/2018/0206/144737_58e25407_1203742.jpeg "wpc2.jpg")


传统的SGS（标准生成器脚本）生成界面，支持SGS语法加亮：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/211615_3436afd6_1203742.png "ts_sgs.png")

Excel生成界面：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/211633_29f2c1ed_1203742.png "ts_excel.png")

Excel模板：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1222/205854_d3391d89_1203742.png "Screenshot from 2018-12-22 20-47-20.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1222/205915_e90163b7_1203742.png "Screenshot from 2018-12-22 20-48-28.png")

在线文档：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/224238_9c8cce80_1203742.png "lt_index.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/224253_96ba778e_1203742.png "lt_10.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/224304_7fe9f3e6_1203742.png "lt_11_2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/224315_879312f8_1203742.png "lt_12.png")

代码生成物截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/210456_48e6fc62_1203742.png "grid.png")

代码生成物多对多界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/210535_9c83adc9_1203742.png "mtm.png")

代码生成物下拉列表截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/211159_07223e09_1203742.png "dropdown.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/211228_b9fa5ce3_1203742.png "dropdown2.png")

代码生成物更新界面截图：
![输入图片说明](https://images.gitee.com/uploads/images/2018/1226/210557_fea96eb0_1203742.png "edit.png")

最新稳定版本和平之翼代码生成器SMEU 3.2.0 正式版。

和平之翼代码生成器SMEU版，一键支持下拉列表和多对多，已支持Oracle数据库。

SMEU技术栈支持JQuery Easy UI,Spring MVC4,spring4, MyBatis 3。

本版支持下拉列表，使用者只需要在域对象相应的外键字段设定dropdown:DomainName fieldName;
即下拉列表：外键域名 字段名，即可一键支持下拉列表（外键）。

本版支持多对多关系，只要在多对多关系的主域对象中定义了
manytomanyslave:slaveDomainName即可在生成的功能和数据库定义中支持了两者的多对多
关系。

和平之翼代码生成器是动词算子式Java通用代码生成器，是无垠式代码生成器的第二代。
修正了一些以前的Bug，并作了功能增强和文档更新。希望您喜欢。
支持Oracle数据库，您只需要定义dbtype:oracle即可支持Oracle数据库，详细情况请看相关示例。

### 翅膀
和平之翼代码生成器图标，翅膀：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1105/161512_b814baaa_1203742.jpeg "peacewing.jpg")